# EthelProduction
This is the repository for the Ethel production code

## Description
Ethel is a platform for teaching and learning, supported by AI.

## Technology Choices

### Frontend

React

### Backend

Nginx with Lua for JWT authentication and FastAPI for handling WebSocket connections and other application logic.

## Author

Gerd Kortemeyer, ETH Zurich, kgerd@ethz.ch

## License
The project is covered by GNU GPL

## Project status
After successful prototype, this enviroment is still under heavy development and nothing runs.
