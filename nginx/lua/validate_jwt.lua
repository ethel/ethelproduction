local jwt = require "resty.jwt"

local SECRET_KEY = "your-secret-key"

local function validate_jwt_token(token)
    local decoded, err = jwt:verify(SECRET_KEY, token)
    if not decoded then
        return false, "Invalid token: " .. err
    end
    if decoded.payload.exp and decoded.payload.exp < os.time() then
        return false, "Token expired"
    end
    return true, decoded.payload
end

local token = ngx.var.http_authorization
if not token or not token:find("Bearer ") then
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.say("Missing or invalid Authorization header")
    ngx.exit(ngx.HTTP_UNAUTHORIZED)
end

token = token:sub(8)
local is_valid, payload_or_err = validate_jwt_token(token)

if not is_valid then
    ngx.status = ngx.HTTP_UNAUTHORIZED
    ngx.say(payload_or_err)
    ngx.exit(ngx.HTTP_UNAUTHORIZED)
end

ngx.req.set_header("X-User-ID", payload_or_err.sub)

