import React, { useState } from 'react';
import axios from 'axios';

const UserSearch = () => {
    const [firstname, setFirstname] = useState('');
    const [result, setResult] = useState(null);
    const [error, setError] = useState(null);

    const handleSearch = async () => {
        try {
            const response = await axios.get(`/api/users/search`, {
                params: {
                    FirstName: firstname,
                },
            });
            setResult(response.data);  // Assuming result contains the data you want to display
            setError(null);  // Reset any previous errors
        } catch (error) {
            setError("User not found or an error occurred");
            setResult(null);
        }
    };

    return (
        <div>
            <h1>Search for User by First Name</h1>
            <input
                type="text"
                value={firstname}
                onChange={(e) => setFirstname(e.target.value)}
                placeholder="Enter first name"
            />
            <button onClick={handleSearch}>Search</button>
            {error && <p style={{ color: 'red' }}>{error}</p>}
            {result && (
                <div>
                    <h3>Search Result:</h3>
                    <pre>{JSON.stringify(result, null, 2)}</pre>
                </div>
            )}
        </div>
    );
};

export default UserSearch;

