import React from 'react';
import UserSearch from './UserSearch';

function App() {
  return (
    <div className="App">
      <UserSearch />
    </div>
  );
}

export default App;

