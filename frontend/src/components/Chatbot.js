// src/components/Chatbot.js
import React, { useState, useEffect } from 'react';

function Chatbot({ initialChatUUID, onChatUpdated }) {
  const [chatHistory, setChatHistory] = useState([]);
  const [currentInput, setCurrentInput] = useState('');
  const [currentBranch, setCurrentBranch] = useState(0);

  // Fetch chat history when component mounts or when the chat UUID changes
  useEffect(() => {
    fetchChatHistory(initialChatUUID);
  }, [initialChatUUID]);

  const fetchChatHistory = async (chatUUID) => {
    try {
      const response = await fetch(`/api/chat/${chatUUID}`);
      const data = await response.json();
      setChatHistory(data.history);
      setCurrentBranch(data.currentBranch);
    } catch (error) {
      console.error('Error fetching chat history:', error);
    }
  };

  const handleInputChange = (e) => {
    setCurrentInput(e.target.value);
  };

  const handleSubmit = async () => {
    if (currentInput.trim() !== '') {
      const newMessage = { content: currentInput, type: 'user' };
      const updatedHistory = [...chatHistory, newMessage];
      setChatHistory(updatedHistory);

      // Call backend to process the input
      try {
        const response = await fetch(`/api/chat/${initialChatUUID}/submit`, {
          method: 'POST',
          body: JSON.stringify({ input: currentInput, branch: currentBranch }),
          headers: { 'Content-Type': 'application/json' },
        });
        const data = await response.json();
        setChatHistory([...updatedHistory, data.response]);
        onChatUpdated(updatedHistory);
      } catch (error) {
        console.error('Error submitting chat input:', error);
      }

      setCurrentInput('');
    }
  };

  return (
    <div className="chatbot-container">
      <div className="chat-history">
        {chatHistory.map((message, index) => (
          <div key={index} className={`chat-message ${message.type}`}>
            {message.content}
          </div>
        ))}
      </div>
      <div className="chat-input">
        <input
          type="text"
          value={currentInput}
          onChange={handleInputChange}
          placeholder="Type your message..."
        />
        <button onClick={handleSubmit}>Send</button>
      </div>
    </div>
  );
}

export default Chatbot;

