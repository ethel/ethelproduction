import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// Example translations
const resources = {
  en: {
    translation: {
      welcome: "Welcome",
      date_format: "{{date, datetime}}"
    }
  },
  fr: {
    translation: {
      welcome: "Bienvenue",
      date_format: "{{date, datetime}}"
    }
  }
};

i18n
  .use(initReactI18next) // Passes i18n instance to react-i18next
  .init({
    resources,
    lng: 'en', // Set the default language to English
    fallbackLng: 'en', // Use English if the current language cannot be found
    interpolation: {
      escapeValue: false, // React already escapes values
      format: function(value, format, lng) {
        if (value instanceof Date) return new Intl.DateTimeFormat(lng).format(value);
        return value;
      }
    }
  });

export default i18n;

