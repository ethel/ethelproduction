# Project Ethel
#
# Low-level routines to directly handle parameter-related entries in the databases
# !!! No permission checking at this level !!!
# Always go through higher-level functions
# !!! NO SQL BEYOND THIS POINT !!!
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from ..db.postgres_connection_pool import get_db_cursor
from psycopg2 import DatabaseError, IntegrityError, OperationalError
from ..db.low_level_database_utils import execute_db_command
import json


# ==============================================================================
# Functions having to do with parameters for resource instances relationship_id
# This is a cascading structure, always the more specific values are in effect
# Parameters are stored as JSON
#
 
# This merges the JSON parameter_value to the existing one
#
def set_parameter(community_id, relationship_id, user_id, parameter_value):
    """
    Adds or updates a parameter row in the ResourceParameters table.
    Allows NULL values for RelationshipID and UserID, and updates JSON structure incrementally.

    Args:
        community_id (UUID): The ID of the community.
        relationship_id (UUID, optional): The ID of the relationship, can be None.
        user_id (UUID, optional): The ID of the user, can be None.
        parameter_value (dict): The dictionary containing the parameter values to be set or updated.

    Returns:
        tuple: (success (bool), message (str))
    """
    try:
        with get_db_cursor(commit=True) as cur:
            # Check if the combination already exists
            check_query = """
            SELECT ParameterID, ParameterValues FROM ResourceParameters
            WHERE CommunityID = %s AND (RelationshipID = %s OR RelationshipID IS NULL) AND (UserID = %s OR UserID IS NULL);
            """
            cur.execute(check_query, (community_id, relationship_id, user_id))
            result = cur.fetchone()

            if result:
                parameter_id, existing_params = result
                # Merge existing JSON with new parameter values
                updated_params = json.loads(existing_params) if existing_params else {}
                updated_params.update(parameter_value)  # Merge dictionaries
                # Update existing parameter
                update_query = """
                UPDATE ResourceParameters
                SET ParameterValues = %s
                WHERE ParameterID = %s;
                """
                cur.execute(update_query, (json.dumps(updated_params), parameter_id))
                logging.info(f"Updated parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter updated successfully"
            else:
                # Insert new parameter
                insert_query = """
                INSERT INTO ResourceParameters (CommunityID, RelationshipID, UserID, ParameterValues)
                VALUES (%s, %s, %s, %s)
                RETURNING ParameterID;
                """
                cur.execute(insert_query, (community_id, relationship_id, user_id, json.dumps(parameter_value)))
                new_parameter_id = cur.fetchone()[0]
                logging.info(f"Added new parameter with ID: {new_parameter_id} for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter added successfully"
    except Exception as e:
        logging.error(f"Failed to set parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}: {e}")
        return False, f"An error occurred: {e}"

# This completely replaces the existing JSON
#
def replace_parameter(community_id, relationship_id, user_id, parameter_value):
    """
    Adds or updates a parameter row in the ResourceParameters table.
    Allows NULL values for RelationshipID and UserID.

    Args:
        community_id (UUID): The ID of the community.
        relationship_id (UUID, optional): The ID of the relationship, can be None.
        user_id (UUID, optional): The ID of the user, can be None.
        parameter_value (JSONB): The JSONB value of the parameter to be set or updated.

    Returns:
        tuple: (success (bool), message (str))
    """
    try:
        with get_db_cursor(commit=True) as cur:
            # Check if the combination already exists
            check_query = """
            SELECT ParameterID FROM ResourceParameters
            WHERE CommunityID = %s AND (RelationshipID = %s OR RelationshipID IS NULL) AND (UserID = %s OR UserID IS NULL);
            """
            cur.execute(check_query, (community_id, relationship_id, user_id))
            existing_id = cur.fetchone()

            if existing_id:
                # Update existing parameter
                update_query = """
                UPDATE ResourceParameters
                SET ParameterValues = %s
                WHERE ParameterID = %s;
                """
                cur.execute(update_query, (parameter_value, existing_id[0]))
                logging.info(f"Updated parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter updated successfully"
            else:
                # Insert new parameter
                insert_query = """
                INSERT INTO ResourceParameters (CommunityID, RelationshipID, UserID, ParameterValues)
                VALUES (%s, %s, %s, %s)
                RETURNING ParameterID;
                """
                cur.execute(insert_query, (community_id, relationship_id, user_id, parameter_value))
                new_parameter_id = cur.fetchone()[0]
                logging.info(f"Added new parameter with ID: {new_parameter_id} for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter added successfully"
    except Exception as e:
        logging.error(f"Failed to set parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}: {e}")
        return False, f"An error occurred: {e}"

# Careful, this completely deletes the existing JSON
#
def del_parameter(community_id, relationship_id, user_id):
    """
    Deletes a parameter row in the ResourceParameters table based on the provided identifiers.

    Args:
        community_id (UUID): The ID of the community.
        relationship_id (UUID, optional): The ID of the relationship, can be None.
        user_id (UUID, optional): The ID of the user, can be None.

    Returns:
        tuple: (success (bool), message (str))
    """
    conditions = ["CommunityID = %s"]
    params = [community_id]

    if relationship_id is not None:
        conditions.append("RelationshipID = %s")
        params.append(relationship_id)
    else:
        conditions.append("RelationshipID IS NULL")

    if user_id is not None:
        conditions.append("UserID = %s")
        params.append(user_id)
    else:
        conditions.append("UserID IS NULL")

    try:
        with get_db_cursor(commit=True) as cur:
            # Check if the row exists
            check_query = f"SELECT ParameterID FROM ResourceParameters WHERE {' AND '.join(conditions)}"
            cur.execute(check_query, tuple(params))
            if cur.fetchone():
                # Delete the row
                delete_query = f"DELETE FROM ResourceParameters WHERE {' AND '.join(conditions)}"
                cur.execute(delete_query, tuple(params))
                logging.info(f"Deleted parameter row for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}")
                return True, "Parameter deleted successfully"
            else:
                logging.warning(f"No parameter found for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'} to delete.")
                return False, "No parameter found to delete"
    except Exception as e:
        logging.error(f"Failed to delete parameter for CommunityID: {community_id}, RelationshipID: {relationship_id if relationship_id else 'None'}, UserID: {user_id if user_id else 'None'}: {e}")
        return False, f"An error occurred: {e}"

# This retrieves the existing JSON
#
def get_parameter(community_id, relationship_id, user_id):
    """
    Retrieves the parameters for a specific community, relationship, and user combination,
    handling None values to match NULL in the database and specific values otherwise.

    Args:
        community_id (UUID or None): ID of the community, None to match NULL.
        relationship_id (UUID or None): ID of the relationship, None to match NULL.
        user_id (UUID or None): ID of the user, None to match NULL.

    Returns:
        tuple: (success (bool), parameters (dict or None))
    """
    # Building the query dynamically to handle None values as SQL NULL and specific values
    conditions = []
    params = []
    if community_id is not None:
        conditions.append("CommunityID = %s")
        params.append(community_id)
    else:
        conditions.append("CommunityID IS NULL")

    if relationship_id is not None:
        conditions.append("RelationshipID = %s")
        params.append(relationship_id)
    else:
        conditions.append("RelationshipID IS NULL")

    if user_id is not None:
        conditions.append("UserID = %s")
        params.append(user_id)
    else:
        conditions.append("UserID IS NULL")

    query = f"""
    SELECT ParameterValues
    FROM ResourceParameters
    WHERE {' AND '.join(conditions)}
    """
    try:
        with get_db_cursor() as cur:
            cur.execute(query, tuple(params))
            result = cur.fetchone()

            if result is not None:
                return True, result[0]  # Return the JSONB stored parameters as a Python dict
            else:
                return True, {}  # Return an empty dictionary if no parameters are found

    except Exception as e:
        logging.error(f"An error occurred while fetching parameters: {e}")
        return False, None

# This assembles the JSON from the complete cascade to get the JSON in effect
#
def get_cascaded_parameters(community_id, relationship_id, user_id):
    """
    Retrieves and merges parameters from the top-level community and relationship down to the specified ones,
    effectively cascading parameters and allowing more specific settings to override general ones.

    Args:
        community_id (UUID): The specific community ID.
        relationship_id (UUID): The specific relationship ID.
        user_id (UUID): The specific user ID.

    Returns:
        tuple: (success (bool), parameters (dict or None))
    """
    try:
        # Start with an empty JSON structure
        cascaded_params = {}

        # Retrieve the community trail
        community_success, community_ids, _ = get_community_relationship_trail(community_id)
        if not community_success:
            return False, None

        # Retrieve the relationship trail
        relationship_success, relationship_ids = get_resource_relationship_trail(relationship_id)
        if not relationship_success:
            return False, None

        # Query for parameters along the community and relationship trail
        parameters_query = """
        SELECT ParameterValues
        FROM ResourceParameters
        WHERE CommunityID = ANY(%s)
          AND RelationshipID = ANY(%s)
          AND UserID = %s
        """
        with get_db_cursor() as cur:
            cur.execute(parameters_query, (community_ids, relationship_ids, user_id))
            results = cur.fetchall()

            # Merge parameters with more specific ones taking precedence
            for result in results:
                # Each result is expected to be a dictionary of parameters
                current_params = result[0]
                # Update the cascaded_params with current_params taking precedence
                cascaded_params = {**cascaded_params, **current_params}

        return True, cascaded_params
    except Exception as e:
        logging.error(f"An error occurred while fetching cascaded parameters: {e}")
        return False, None

