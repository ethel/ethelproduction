# Project Ethel
#
# RESTful command processor
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

from fastapi import FastAPI

from cmd_service.user.router import router as user_router
from cmd_service.community.router import router as community_router
from cmd_service.auth.router import router as auth_router
from cmd_service.parameter.router import router as parameter_router
from cmd_service.resource.router import router as resource_router

app = FastAPI()

# Register routers with prefixes
app.include_router(user_router, prefix="/user", tags=["user"])
app.include_router(community_router, prefix="/community", tags=["community"])
app.include_router(auth_router, prefix="/auth", tags=["auth"])
app.include_router(parameter_router, prefix="/parameter", tags=["parameter"])
app.include_router(resource_router, prefix="/resource", tags=["resource"])
