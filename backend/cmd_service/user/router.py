# Project Ethel
#
# Router for user commands
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#

from fastapi import APIRouter, HTTPException
from .low_level_user_utils import search_user_by_field

router = APIRouter()

@router.get("/search")
async def search_user(FirstName: str):
    success, result = search_user_by_field(3, FirstName=FirstName)
    if not success:
        raise HTTPException(status_code=404, detail="User not found")
    return result

