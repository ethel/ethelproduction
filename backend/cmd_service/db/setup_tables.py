# Project Ethel
#
# Make tables if necessary
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from .postgres_connection_pool import get_db_cursor
from psycopg2 import DatabaseError

def setup_database():
    """
    Sets up database tables if they are not already present. This function creates
    tables and database extensions necessary for the application to function.
    """
    logging.info("Set up database tables if they aren't there ...")
    try:
        with get_db_cursor(commit=True) as cur:
            # List of SQL statements to execute
            commands = [
                """
                CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
                """,
                """
                CREATE EXTENSION IF NOT EXISTS pg_trgm;
                """,
                """
                CREATE TABLE IF NOT EXISTS Resources (
                    ResourceID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    ResourceName VARCHAR(255) NOT NULL,
                    ResourceType VARCHAR(255) NOT NULL,
                    ResourceUsed BOOLEAN NOT NULL DEFAULT FALSE
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS Communities (
                    CommunityID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    ParentCommunityID UUID REFERENCES Communities(CommunityID),
                    CommunityName VARCHAR(255) NOT NULL,
                    TopLevelResourceID UUID REFERENCES Resources(ResourceID)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS Users (
                    UserID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    FirstName VARCHAR(255) NOT NULL,
                    FirstNameTranscription VARCHAR(255),
                    MiddleNames VARCHAR(255),
                    MiddleNamesTranscription VARCHAR(255),
                    LastName VARCHAR(255) NOT NULL,
                    LastNameTranscription VARCHAR(255),
                    GenerationQualifier VARCHAR(50),
                    GenerationQualifierTranscription VARCHAR(50),
                    TopLevelResourceID UUID REFERENCES Resources(ResourceID)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS CommunityUserIDs (
                    CommunityUserID VARCHAR(255),
                    CommunityID UUID,
                    UserID UUID,
                    PRIMARY KEY (CommunityUserID, CommunityID),
                    FOREIGN KEY (CommunityID) REFERENCES Communities(CommunityID),
                    FOREIGN KEY (UserID) REFERENCES Users(UserID)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS Roles (
                    RoleID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    RoleName VARCHAR(255) NOT NULL
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS UserRoles (
                    UserID UUID,
                    RoleID UUID,
                    CommunityID UUID,
                    StartDate TIMESTAMP WITH TIME ZONE,
                    EndDate TIMESTAMP WITH TIME ZONE,
                    PRIMARY KEY (UserID, RoleID, CommunityID),
                    FOREIGN KEY (UserID) REFERENCES Users(UserID),
                    FOREIGN KEY (RoleID) REFERENCES Roles(RoleID),
                    FOREIGN KEY (CommunityID) REFERENCES Communities(CommunityID)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS AuthenticationTypes (
                    AuthTypeID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    AuthTypeName VARCHAR(255) NOT NULL UNIQUE
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS AuthenticationUserIDs (
                    AuthUserID VARCHAR(255),
                    AuthTypeID UUID,
                    UserID UUID,
                    PRIMARY KEY (AuthUserID, AuthTypeID),
                    FOREIGN KEY (AuthTypeID) REFERENCES AuthenticationTypes(AuthTypeID),
                    FOREIGN KEY (UserID) REFERENCES Users(UserID)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS ResourceRelationships (
                    RelationshipID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    ParentResourceID UUID REFERENCES Resources(ResourceID),
                    ChildResourceID UUID REFERENCES Resources(ResourceID),
                    ChildOrder BIGINT,
                    DisplayName VARCHAR(255),
                    IsAssessment BOOLEAN,
                    UNIQUE (ParentResourceID, ChildResourceID, ChildOrder)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS ResourceParameters (
                    ParameterID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    CommunityID UUID NOT NULL,
                    RelationshipID UUID,
                    UserID UUID,
                    ParameterValues JSONB,
                    FOREIGN KEY (CommunityID) REFERENCES Communities(CommunityID),
                    FOREIGN KEY (RelationshipID) REFERENCES ResourceRelationships(RelationshipID),
                    FOREIGN KEY (UserID) REFERENCES Users(UserID),
                    UNIQUE (CommunityID, RelationshipID, UserID)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS ResourceData (
                    RelationshipID UUID NOT NULL,
                    UserID UUID NOT NULL,
                    MaxCredit FLOAT,
                    CurrentCredit FLOAT,
                    LastModified TIMESTAMP WITH TIME ZONE,
                    InternalData JSONB NOT NULL,
                    FOREIGN KEY (RelationshipID) REFERENCES ResourceRelationships(RelationshipID),
                    FOREIGN KEY (UserID) REFERENCES Users(UserID),
                    PRIMARY KEY (RelationshipID, UserID)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS BotInstance (
                    BotInstanceID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    BotInstanceName VARCHAR(255) NOT NULL
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS Chats (
                    ChatID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    UserID UUID NOT NULL REFERENCES Users(UserID),
                    CommunityID UUID NOT NULL REFERENCES Communities(CommunityID),
                    BotInstanceID UUID NOT NULL REFERENCES BotInstance(BotInstanceID),
                    CreationDate TIMESTAMP WITH TIME ZONE NOT NULL,
                    LastUpdatedDate TIMESTAMP WITH TIME ZONE NOT NULL,
                    CurrentNodeID UUID,
                    RootNodeID UUID NOT NULL,
                    ChatTitle VARCHAR(255) NOT NULL,
                    IsVisible BOOLEAN NOT NULL DEFAULT TRUE
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS ChatNodes (
                    NodeID UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
                    ChatID UUID NOT NULL REFERENCES Chats(ChatID),
                    NodeType VARCHAR(10) NOT NULL CHECK (NodeType IN ('User', 'AI','Root')),
                    Data JSONB NOT NULL,
                    CreationDate TIMESTAMP WITH TIME ZONE NOT NULL,
                    UNIQUE (NodeID, ChatID)
                );
                """,
                """
                CREATE TABLE IF NOT EXISTS ChatNodeLinks (
                    ChatID UUID NOT NULL,
                    ParentNodeID UUID NOT NULL,
                    ChildNodeID UUID NOT NULL,
                    PRIMARY KEY (ParentNodeID, ChildNodeID),
                    FOREIGN KEY (ChatID) REFERENCES Chats(ChatID),
                    FOREIGN KEY (ParentNodeID, ChatID) REFERENCES ChatNodes(NodeID, ChatID),
                    FOREIGN KEY (ChildNodeID, ChatID) REFERENCES ChatNodes(NodeID, ChatID)
                );
                """,
                """
                CREATE INDEX IF NOT EXISTS idx_gin_firstname ON Users USING GIN (FirstName gin_trgm_ops);
                """,
                """
                CREATE INDEX IF NOT EXISTS idx_gin_lastname ON Users USING GIN (LastName gin_trgm_ops);
                """,
                """
                CREATE INDEX IF NOT EXISTS idx_gin_auth_typename ON AuthenticationTypes USING GIN (AuthTypeName gin_trgm_ops);
                """,
                """
                CREATE INDEX IF NOT EXISTS idx_gin_community_name ON Communities USING GIN (CommunityName gin_trgm_ops);
                """,
                """
                CREATE INDEX IF NOT EXISTS idx_chatnodes_chatid ON ChatNodes(ChatID);
                """,
                """
                CREATE INDEX IF NOT EXISTS idx_chats_currentnodeid ON Chats(CurrentNodeID);
                """,
                """
                CREATE INDEX IF NOT EXISTS idx_chats_rootnodeid ON Chats(RootNodeID);
                """
            ]
            for command in commands:
                cur.execute(command)
            logging.info("Database tables created or verified successfully.")
    except DatabaseError as e:
        logging.error(f"Database error: {e}")
        raise RuntimeError("Database setup failed")  # Propagate error as an exception
