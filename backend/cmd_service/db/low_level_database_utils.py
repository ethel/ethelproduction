# Project Ethel
#
# Low-level routines to directly handle the databases
# !!! No permission checking at this level !!!
# Function to execute database commands
#
# Copyright (C) 2024 Gerd Kortemeyer, ETH Zurich
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from .postgres_connection_pool import get_db_cursor
from psycopg2 import DatabaseError, IntegrityError, OperationalError

#
# Function to execute single SQL commands
#
def execute_db_command(sql, params=None):
    try:
        with get_db_cursor(commit=True) as cur:
            cur.execute(sql, params or ())
            if sql.strip().upper().startswith("SELECT") or "RETURNING" in sql.upper():
                results = cur.fetchall()  # Retrieve all rows from SELECT
                return True, results
            else:
                return True, "Command executed successfully"  # For UPDATE, DELETE where no return is expected
    except IntegrityError as e:
        return False, f"Failed to insert data due to integrity constraints: {e}"
    except OperationalError as e:
        return False, f"Failed to connect or execute command: {e}"
    except DatabaseError as e:
        return False, f"General database error: {e}"
    except Exception as e:
        return False, f"An unexpected error occurred: {e}"
