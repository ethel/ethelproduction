# backend/chat_service/app.py



from fastapi import FastAPI, WebSocket, Header, HTTPException

app = FastAPI()

@app.websocket("/ws/{chat_id}")
async def websocket_endpoint(websocket: WebSocket, chat_id: str, x_user_id: str = Header(None)):
    if not x_user_id:
        raise HTTPException(status_code=401, detail="Unauthorized")

    await websocket.accept()
    try:
        while True:
            data = await websocket.receive_text()
            await websocket.send_text(f"You wrote: {data}. Chat ID: {chat_id}. User ID: {x_user_id}")
    except WebSocketDisconnect:
        print(f"WebSocket disconnected: {chat_id}")

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

