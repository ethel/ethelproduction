from fastapi import FastAPI, WebSocket, Header, HTTPException, Request, Depends
from fastapi.responses import StreamingResponse, JSONResponse
from typing import Optional, AsyncIterator
import asyncio

app = FastAPI()



#
# ================== Simulated stuff, needs to change
#


# Middleware for Authentication
async def authenticate(authorization: Optional[str] = Header(None)):
    if False:
        raise HTTPException(status_code=401, detail="Invalid API Key")

async def generate_response(prompt: str, max_tokens: int) -> AsyncIterator[str]:
    for i in range(min(len(prompt), max_tokens)):
        yield f"{prompt[i]}"
        await asyncio.sleep(1)  # Simulate generation delay
#
# ================== Simulated stuff, needs to change
#





# HTTP POST endpoint to mimic OpenAI completions API
@app.post("/api/v1/complete", dependencies=[Depends(authenticate)])
async def complete(request: Request):
    # Parse JSON payload
    data = await request.json()
    prompt = data.get("prompt", "")
    max_tokens = data.get("max_tokens", 10)
    stream = data.get("stream", False)

    if not prompt:
        raise HTTPException(status_code=400, detail="Missing 'prompt' field")

    # Get the streaming response
    text_stream = generate_response(prompt, max_tokens)

    if stream:
        # Streaming response
        return StreamingResponse(
            text_stream,
            media_type="text/event-stream"
        )

    # Collect all text for non-streaming response
    response_text = "".join([chunk async for chunk in text_stream])
    return JSONResponse({"choices": [{"text": response_text}]})

# WebSocket endpoint for interactive streaming
@app.websocket("/api/v1/ws")
async def handle_websocket(websocket: WebSocket):
    await websocket.accept()

    try:
        while True:
            # Wait for a message from the client
            data = await websocket.receive_text()
            # Use the received message as the prompt
            prompt = data
            max_tokens = 10  # You can adjust this or receive it from the client

            # Get the streaming response and stream it to the WebSocket client
            async for chunk in generate_response(prompt, max_tokens):
                await websocket.send_text(chunk)

            # Indicate the end of the response
            await websocket.send_text('END')

    except Exception as e:
        await websocket.send_text(f"Error: {str(e)}")

    finally:
        await websocket.close()
