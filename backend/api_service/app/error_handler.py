import logging

logger = logging.getLogger(__name__)

def handle_error(error, error_handling, state):
    """
    Process errors according to the task's error_handling configuration.
    Returns a dict with the action to take (e.g., goto, halt, or continue).
    """
    logger.debug(f"Handling error: {error} with config: {error_handling}")
    action = error_handling.get("on_error")
    if action:
        return action
    return None

