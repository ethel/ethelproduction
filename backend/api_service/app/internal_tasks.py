"""
internal_tasks.py

This module implements a registry for internal tasks that can be executed asynchronously.
Each internal task is registered under a name so that when the workflow engine receives
an internal task (e.g. "from_template_builder" or "handle_error_function"), it can look up and call
the corresponding function.
"""

import asyncio
import logging
import os

logger = logging.getLogger(__name__)

# Global registry dictionary for internal tasks.
INTERNAL_TASKS = {}

def register_internal_task(name):
    """
    Decorator that registers an internal task under the given name.
    """
    def decorator(func):
        INTERNAL_TASKS[name] = func
        return func
    return decorator

@register_internal_task("from_template_builder")
async def from_template_builder(task_params):
    """
    Asynchronously read a template text file and replace placeholders using values from a provided dict.
    
    Expected task_params:
      - "filename": The path to the template file.
      - "values": A dict mapping placeholder names to replacement values.
    
    Example template file content:
      "Hello, {name}! Today is {date}."
      
    Returns:
      A dict: { "filled_template": "<resulting text>" }
    """
    filename = task_params.get("filename")
    values = task_params.get("values", {})

    if not filename:
        raise ValueError("Missing 'filename' parameter for from_template_builder task.")

    if not os.path.exists(filename):
        raise FileNotFoundError(f"Template file '{filename}' does not exist.")

    # Define a synchronous function to read the file.
    def read_file():
        with open(filename, 'r') as f:
            return f.read()

    try:
        # Use asyncio.to_thread to run the synchronous read_file() without blocking.
        template_text = await asyncio.to_thread(read_file)
    except Exception as e:
        logger.error(f"Error reading template file '{filename}': {e}")
        raise

    try:
        # Perform placeholder substitution using Python's format method.
        filled_text = template_text.format(**values)
    except Exception as e:
        logger.error(f"Error performing template substitution: {e}")
        raise ValueError(f"Template substitution failed: {e}")

    logger.debug(f"Template '{filename}' processed successfully.")
    return {"filled_template": filled_text}

@register_internal_task("handle_error_function")
async def handle_error_function(task_params):
    """
    A dummy error handling function.
    Simply returns a marker indicating that an error was handled.
    """
    logger.debug("Executing dummy handle_error_function with params: %s", task_params)
    return {"error_message": "Error was handled."}

def get_internal_task(task_name):
    """
    Retrieve an internal task function by its name.
    Returns the function if found, or None if not registered.
    """
    return INTERNAL_TASKS.get(task_name)

