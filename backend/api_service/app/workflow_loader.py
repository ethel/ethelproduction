import json
from jsonschema import validate, ValidationError


def load_schema(schema_path):
    """
    Load and return the JSON schema from the given file path.
    
    :param schema_path: Path to the JSON schema file.
    :return: The loaded JSON schema as a Python dict.
    """
    with open(schema_path, 'r') as f:
        schema = json.load(f)
    return schema


def load_workflow(workflow_path):
    """
    Load and return the workflow JSON from the given file path.
    
    :param workflow_path: Path to the workflow JSON file.
    :return: The loaded workflow as a Python dict.
    """
    with open(workflow_path, 'r') as f:
        workflow = json.load(f)
    return workflow


def validate_workflow(workflow, schema):
    """
    Validate the workflow JSON against the provided schema.
    
    :param workflow: The workflow JSON as a Python dict.
    :param schema: The JSON schema as a Python dict.
    :raises ValidationError: if the workflow does not conform to the schema.
    """
    validate(instance=workflow, schema=schema)


# For testing purposes if you run this module directly:
if __name__ == "__main__":
    import argparse
    import sys

    parser = argparse.ArgumentParser(
        description="Load and validate a workflow JSON file against a JSON schema."
    )
    parser.add_argument(
        "workflow_file",
        help="Path to the workflow JSON file (e.g. my_workflow.json)"
    )
    parser.add_argument(
        "--schema",
        default="ethel_taskflow_schema.json",
        help="Path to the JSON schema file (default: ethel_taskflow_schema.json)"
    )
    args = parser.parse_args()

    try:
        schema = load_schema(args.schema)
        workflow = load_workflow(args.workflow_file)
        validate_workflow(workflow, schema)
        print("Workflow is valid!")
    except ValidationError as e:
        print("Workflow validation failed:")
        print(e)
        sys.exit(1)
    except Exception as ex:
        print("Error:", ex)
        sys.exit(1)

