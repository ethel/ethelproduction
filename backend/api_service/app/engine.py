import asyncio
import logging

from task import create_task_object
from condition_evaluator import evaluate_boolean_expression
from state_manager import update_state, apply_output_mapping
from error_handler import handle_error

logger = logging.getLogger(__name__)

class WorkflowEngine:
    def __init__(self, workflow):
        self.workflow = workflow
        self.state = workflow.get("initial_state", {})
        self.stages = workflow.get("stages", [])
        self.current_stage_index = 0

    async def run(self):
        logger.info("Starting workflow execution.")
        while self.current_stage_index < len(self.stages):
            stage = self.stages[self.current_stage_index]
            stage_id = stage.get("stage_id")
            logger.debug(f"Starting stage: {stage_id}")

            await self.execute_stage(stage)

            next_index = self.get_next_stage_index(stage)
            if next_index is not None:
                self.current_stage_index = next_index
            else:
                self.current_stage_index += 1

        logger.info("Workflow execution complete.")
        return self.state

    async def execute_stage(self, stage):
        tasks = stage.get("tasks", [])
        for task_def in tasks:
            task_obj = create_task_object(task_def)
            try:
                result = await task_obj.execute(self.state)
                # Check if the result is an async generator (streaming output)
                if hasattr(result, '__aiter__'):
                    if stage.get("stream_stage_output", False):
                        # In streaming mode: send chunks directly to client (simulate by printing)
                        logger.debug(f"Streaming output for task {task_obj.task_id}:")
                        async for chunk in result:
                            # In production, you would pipe the chunk to the client's HTTP response.
                            print(chunk)
                        # Optionally update the state with a success marker.
                        update_state(self.state, {"stream_status": "success"})
                    else:
                        # Testing mode: accumulate streaming chunks into a list and then update state.
                        chunks = []
                        async for chunk in result:
                            chunks.append(chunk)
                        result = chunks
                        mapped_output = apply_output_mapping(task_obj.output_mapping, result)
                        update_state(self.state, mapped_output)
                else:
                    # For non-streaming tasks, simply apply output mapping and update state.
                    mapped_output = apply_output_mapping(task_obj.output_mapping, result)
                    update_state(self.state, mapped_output)
                logger.debug("State after stage %s: %s", stage.get("stage_id"), self.state)
            except Exception as e:
                logger.error(f"Error in task {task_obj.task_id}: {e}")
                action = handle_error(e, task_obj.error_handling, self.state)
                if action and action.get("action") == "goto":
                    target = action.get("target_stage")
                    idx = self.find_stage_index_by_id(target)
                    if idx is not None:
                        self.current_stage_index = idx
                        return
                elif action and action.get("action") == "halt":
                    raise Exception("Workflow halted due to task error.")
                # Otherwise, continue to the next task.

    def get_next_stage_index(self, stage):
        # Evaluate conditional transitions.
        conditionals = stage.get("conditionals", [])
        for cond in conditionals:
            expr = cond.get("if")
            if evaluate_boolean_expression(expr, self.state):
                target = cond.get("goto")
                idx = self.find_stage_index_by_id(target)
                if idx is not None:
                    return idx

        # Check for an unconditional transition.
        transitions = stage.get("transitions", {})
        if "unconditional" in transitions:
            action = transitions["unconditional"]
            if action.get("action") == "goto":
                target = action.get("target_stage")
                idx = self.find_stage_index_by_id(target)
                if idx is not None:
                    return idx

        return None

    def find_stage_index_by_id(self, stage_id):
        for idx, s in enumerate(self.stages):
            if s.get("stage_id") == stage_id:
                return idx
        logger.error(f"Stage '{stage_id}' not found!")
        return None

