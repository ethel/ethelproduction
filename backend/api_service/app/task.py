import asyncio
import logging
import random

from external_services import call_microservice, call_llm
from internal_tasks import get_internal_task
from state_manager import apply_input_mapping
from genai_tools import get_genai_tool

logger = logging.getLogger(__name__)

class BaseTask:
    def __init__(self, task_def):
        self.task_def = task_def
        # Use task_id for regular tasks or group_id for groups.
        self.task_id = task_def.get("task_id", task_def.get("group_id", ""))
        self.type = task_def.get("type")
        self.input_mapping = task_def.get("input_mapping", {})
        self.output_mapping = task_def.get("output_mapping", {})
        self.timeout = task_def.get("timeout", None)
        self.error_handling = task_def.get("error_handling", {})
        self.stream_output = task_def.get("stream_output", False)

    async def execute(self, state):
        raise NotImplementedError("Subclasses must implement execute()")

class TaskItem(BaseTask):
    async def execute(self, state):
        logger.debug(f"Executing task {self.task_id} of type {self.type}")
        if self.type == "microservice":
            result = await call_microservice(self.task_def, state)
        elif self.type == "llm":
            result = await call_llm(self.task_def, state)
        elif self.type == "internal":
            internal_func = get_internal_task(self.task_def.get("function"))
            if not internal_func:
                raise Exception(f"Internal task function '{self.task_def.get('function')}' is not registered.")
            params = apply_input_mapping(self.task_def.get("input_mapping", {}), state)
            delay = random.uniform(0.1, 0.3)
            logger.debug(f"[Internal {self.task_id}] Simulated call with {delay:.2f} sec delay.")
            await asyncio.sleep(delay)
            result = await internal_func(params)
        elif self.type == "genAI":
            # Look up the GenAI tool by its abstract name.
            tool = get_genai_tool(self.task_def.get("function"))
            payload = apply_input_mapping(self.task_def.get("input_mapping", {}), state)
            result = await tool.call(payload)
        else:
            raise Exception(f"Unknown task type: {self.type}")
        return result

class TaskGroup(BaseTask):
    def __init__(self, task_def):
        super().__init__(task_def)
        self.tasks = [create_task_object(subtask) for subtask in task_def.get("tasks", [])]

    async def execute(self, state):
        logger.debug(f"Executing task group {self.task_id} in parallel")
        coros = [task.execute(state) for task in self.tasks]
        results = await asyncio.gather(*coros, return_exceptions=True)
        combined = {}
        for res in results:
            if isinstance(res, Exception):
                logger.error(f"Error in task group {self.task_id}: {res}")
            elif isinstance(res, dict):
                combined.update(res)
        return combined

def create_task_object(task_def):
    if "group_id" in task_def:
        return TaskGroup(task_def)
    else:
        return TaskItem(task_def)

