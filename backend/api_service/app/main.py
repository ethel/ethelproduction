import asyncio
import logging
from workflow_loader import load_schema, load_workflow, validate_workflow
from engine import WorkflowEngine
from genai_tools import load_genai_config
load_genai_config("genai_config.json")

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

def main():
    import argparse

    parser = argparse.ArgumentParser(
        description="Run a workflow using the Workflow Engine."
    )
    parser.add_argument(
        "workflow_file",
        help="Path to the workflow JSON file"
    )
    parser.add_argument(
        "--schema",
        default="ethel_taskflow_schema.json",
        help="Path to the JSON schema file (default: ethel_taskflow_schema.json)"
    )
    args = parser.parse_args()

    try:
        schema = load_schema(args.schema)
        workflow = load_workflow(args.workflow_file)
        validate_workflow(workflow, schema)
        logger.info("Workflow validated successfully.")

        engine = WorkflowEngine(workflow)
        final_state = asyncio.run(engine.run())
        logger.info(f"Final state: {final_state}")
    except Exception as e:
        logger.error(f"Workflow execution error: {e}")

if __name__ == "__main__":
    main()

