def apply_input_mapping(mapping, state):
    """
    Given a mapping and the current state, produce the input for a task.
    Supports nested mappings.
    """
    result = {}
    for key, value in mapping.items():
        if isinstance(value, dict):
            result[key] = apply_input_mapping(value, state)
        else:
            result[key] = state.get(value)
    return result

def apply_output_mapping(mapping, output):
    """
    Map the task’s output into a dictionary using the provided output mapping.
    
    The mapping is defined as:
    
        { "task_output_field": "state_field" }
    
    This means: take the value of 'task_output_field' from the output and store it in the state under 'state_field'.
    
    If the output is a list of dictionaries (as for streaming output), then for each mapping entry,
    gather the values from each dictionary into a list.
    """
    result = {}
    if isinstance(output, list):
        # For each mapping pair, gather the values from each dictionary in the list.
        for task_field, state_field in mapping.items():
            result[state_field] = [item.get(task_field) for item in output if isinstance(item, dict)]
    elif isinstance(output, dict):
        for task_field, state_field in mapping.items():
            result[state_field] = output.get(task_field)
    else:
        result = output
    return result

def update_state(state, updates):
    """
    Update the current state with the provided updates.
    """
    state.update(updates)
    return state

