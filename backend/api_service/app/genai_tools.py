import os
import json
import asyncio
import logging
from abc import ABC, abstractmethod

logger = logging.getLogger(__name__)

# Global configuration for GenAI tools.
GENAI_CONFIG = {}

def load_genai_config(config_path):
    """
    Load the centralized GenAI configuration from a JSON file.
    This function must be called (e.g., from main.py) before any tool is instantiated.
    """
    global GENAI_CONFIG
    with open(config_path, 'r') as f:
        GENAI_CONFIG = json.load(f)
    logger.info("GenAI configuration loaded from %s", config_path)

def get_tool_config(tool_name):
    """
    Retrieve the configuration for the specified tool.
    Returns an empty dict if not found.
    """
    return GENAI_CONFIG.get(tool_name, {})

# Registry for GenAI tools.
GENAI_TOOL_REGISTRY = {}

def register_genai_tool(name):
    """
    Decorator to register a GenAITool implementation under a given name.
    """
    def decorator(cls):
        GENAI_TOOL_REGISTRY[name] = cls
        cls._tool_name = name  # store the registration name on the class
        return cls
    return decorator

def get_genai_tool(name):
    """
    Retrieve an instance of the GenAITool associated with the given name.
    """
    tool_cls = GENAI_TOOL_REGISTRY.get(name)
    if not tool_cls:
        raise ValueError(f"GenAI tool '{name}' is not registered.")
    return tool_cls()

class GenAITool(ABC):
    """
    Abstract base class for GenAI tools.
    Each concrete tool must implement the call() method.
    """
    def __init__(self):
        # Retrieve configuration for this tool using its registration name.
        config = get_tool_config(self._tool_name)
        self.endpoint = config.get("endpoint")
        self.secret_key = config.get("secret_key")
        self.protocol = config.get("protocol", "https")
        self.streaming = config.get("streaming", False)

    @abstractmethod
    async def call(self, payload):
        """
        Call the GenAI service with the provided payload.
        For streaming tools, return an async generator.
        For non-streaming tools, return the final result.
        """
        pass

@register_genai_tool("LLM_mini")
class LLMMini(GenAITool):
    def __init__(self):
        super().__init__()

    async def call(self, payload):
        # Simulate a network call with asynchronous delay.
        await asyncio.sleep(1)
        # Return a dummy structured JSON response.
        return {"text": f"LLM_mini response for payload: {payload}"}

@register_genai_tool("LLM_standard")
class LLMStandard(GenAITool):
    def __init__(self):
        super().__init__()

    async def call(self, payload):
        # Simulate a streaming response as an async generator.
        async def stream_response():
            for i in range(3):
                await asyncio.sleep(0.5)
                yield {"chunk": f"LLM_standard chunk {i} for payload: {payload}"}
        return stream_response()

@register_genai_tool("ImageGenerator")
class ImageGenerator(GenAITool):
    def __init__(self):
        super().__init__()

    async def call(self, payload):
        # Simulate a network call with asynchronous delay.
        await asyncio.sleep(1.5)
        prompt = payload.get("prompt", "default")
        # Use the endpoint from configuration to build the image URL.
        return {"image_url": f"{self.endpoint}/generated/{prompt}.png"}

@register_genai_tool("Embedding_V2")
class EmbeddingV2(GenAITool):
    def __init__(self):
        super().__init__()

    async def call(self, payload):
        await asyncio.sleep(0.8)
        return {"embedding": [0.1, 0.2, 0.3]}

@register_genai_tool("Embedding_V3")
class EmbeddingV3(GenAITool):
    def __init__(self):
        super().__init__()

    async def call(self, payload):
        await asyncio.sleep(0.8)
        return {"embedding": [0.4, 0.5, 0.6]}

