from fastapi import FastAPI, Request, HTTPException, Depends
from fastapi.responses import RedirectResponse
from jose import jwt, JWTError
from datetime import datetime, timedelta
import logging

app = FastAPI()

# Replace with your actual secret key
SECRET_KEY = "your-secret-key"
ALGORITHM = "RS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

# Mock configuration for the demo
MOCK_OIDC_REDIRECT_URI = "http://localhost/auth"
MOCK_OIDC_CLIENT_ID = "mock-client-id"
MOCK_OIDC_TOKEN_ENDPOINT = "mock-token-endpoint"

# Dummy private and public keys (replace with your actual keys)
PRIVATE_KEY = """-----BEGIN PRIVATE KEY-----
YOUR_PRIVATE_KEY_HERE
-----END PRIVATE KEY-----"""

PUBLIC_KEY = """-----BEGIN PUBLIC KEY-----
YOUR_PUBLIC_KEY_HERE
-----END PUBLIC KEY-----"""

@app.get("/auth")
async def authenticate_user(code: str = None):
    if not code:
        logging.info("No code provided")
        raise HTTPException(status_code=400, detail="Authorization code is required")

    # For the mock service, we'll just assume the code is valid
    try:
        # Mock user info retrieval and token generation
        user_info = {"sub": "1234567890", "name": "John Doe", "email": "johndoe@example.com"}
        access_token = create_access_token(data=user_info)

        # Issue the JWT and redirect to the app
        response = RedirectResponse(url="/app")
        response.set_cookie(key="ethel_session", value=access_token, httponly=True, secure=False)
        return response
    except Exception as e:
        logging.error(f"Authentication failed: {e}")
        raise HTTPException(status_code=500, detail="Internal server error")


@app.get("/app")
async def app_landing_page():
    # This is where your actual app would live
    return {"message": "Welcome to the app!"}

def create_access_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, PRIVATE_KEY, algorithm=ALGORITHM)
    return encoded_jwt

def verify_access_token(token: str):
    try:
        payload = jwt.decode(token, PUBLIC_KEY, algorithms=[ALGORITHM])
        return payload
    except JWTError:
        raise HTTPException(status_code=401, detail="Could not validate credentials")

@app.get("/validate-token")
async def validate_token(request: Request):
    token = request.cookies.get("ethel_session")
    if not token:
        raise HTTPException(status_code=401, detail="Not authenticated")
    
    user_info = verify_access_token(token)
    return {"user_info": user_info}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

